﻿namespace _006_AutomapperWithProjection
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;

    using AutoMapper;
    using AutoMapper.QueryableExtensions;

    using Database;

    using Models;

    class Program
    {
        static void Main(string[] args)
        {
            ConfigureAutomapper();

            var db = new AutomapperDatabase();

            db.Database.Log = s => Console.WriteLine(s);

            var persons =
                db.Persons.Include(p => p.Addresses)
                    .Include(p => p.Telephones)
                    .Where(p => p.Addresses.Any())
                    .Where(p => p.Telephones.Any())
                    .ProjectTo<PersonConsoleDisplay>()
                    .Take(100)
                    .ToList();

            var consolePersons = Mapper.Map<List<PersonConsoleDisplay>, List<ConsoleReadyPerson>>(persons);


            foreach (var person in consolePersons)
            {
                Console.WriteLine(
                    $"Name:{person.Name.PadRight(20)}\t\t From:{person.Country.PadRight(20)}\t\t Has phone: {person.Phone.PadRight(20)}");
            }
        }

        private static void ConfigureAutomapper()
        {
            Mapper.Initialize(
                c =>
                {
                    c.CreateMap<Person, PersonConsoleDisplay>()
                        .ForMember(d => d.FirstName, o => o.MapFrom(s => s.FirstName))
                        .ForMember(d => d.Countries, o => o.MapFrom(s => s.Addresses.Select(a => a.Country)))
                        .ForMember(d => d.Phone, o => o.MapFrom(s => s.Telephones.Select(t => t.PhoneType.ToString())));

                    c.CreateMap<PersonConsoleDisplay, ConsoleReadyPerson>()
                        .ForMember(d => d.Name, o => o.MapFrom(s => s.FirstName))
                        .ForMember(d => d.Country, o => o.MapFrom(s => s.Countries.First()))
                        .ForMember(d => d.Phone, o => o.MapFrom(s => s.Phone.First()));
                });

        }
    }
}
