﻿namespace _000_Automapper
{
    using Models;

    public class Program
    {
        static void Main(string[] args)
        {
            var databaseModel = new DatabaseModel();
            var viewModel = new ViewModel();

            viewModel.FirstName = databaseModel.FirstName;
            viewModel.LastName = databaseModel.LastName;
            viewModel.Id = databaseModel.Id;
            viewModel.Address = databaseModel.Address;
        }
    }
}
