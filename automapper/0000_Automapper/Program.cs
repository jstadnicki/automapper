﻿namespace _0000_Automapper
{
    using AutoMapper;

    using Models;

    class Program
    {
        static void Main(string[] args)
        {
            Mapper.Initialize(c =>
            {
                c.CreateMap<DatabaseModel, ViewModel>();
            });


            var databaseModel = new DatabaseModel();
            var viewModel = Mapper.Map<DatabaseModel, ViewModel>(databaseModel);
        }
    }
}
