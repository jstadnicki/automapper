﻿namespace _007_ComplexMapping
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;

    using AutoMapper;
    using AutoMapper.QueryableExtensions;

    using Database;

    using Models;

    public class Program
    {
        static void Main(string[] args)
        {
            ConfigureAutomapper();

            var db = new AutomapperDatabase();

            db.Database.Log = s => Console.WriteLine(s);

            var persons =
                db.Persons.Include(p => p.Addresses)
                    .Include(p => p.Telephones)
                    .Where(p => p.Addresses.Any())
                    .Where(p => p.Telephones.Any())
                    .Take(100)
                    .ToList();

            var personsToDisplay = Mapper.Map<List<Person>, List<ConsoleReadyPerson>>(persons);

            foreach (var person in personsToDisplay)
            {
                Console.WriteLine(
                    $"Name:{person.Name.PadRight(30)}\t\t From:{person.Country.PadRight(20)}\t\t Has phone: {person.Phone.PadRight(20)}");
            }
        }

        private static void ConfigureAutomapper()
        {
            Mapper.Initialize(
                c =>
                {
                    c.CreateMap<Person, ConsoleReadyPerson>()
                    .ForMember(d => d.Name, o => o.ResolveUsing<PersonNameResolver>())
                    .ForMember(d => d.Country, o => o.MapFrom(s => s.Addresses.Select(a => a.Country).First()))
                    .ForMember(
                        d => d.Phone,
                        o => o.MapFrom(s => s.Telephones.Select(p => p.PhoneType.ToString()).First()));
                });
        }
    }

    internal class PersonNameResolver : IValueResolver<Person, ConsoleReadyPerson, string>
    {
        public string Resolve(
            Person source,
            ConsoleReadyPerson destination,
            string destMember,
            ResolutionContext context)
        {
            return string.Concat($"Imię: {source.FirstName} Nazwisko: {source.LastName}".Reverse());
        }
    }
}
