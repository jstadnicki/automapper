﻿namespace Models
{
    public enum AddressType
    {
        Unknown = 0,
        Home,
        Company,
        Correspondence
    }
}