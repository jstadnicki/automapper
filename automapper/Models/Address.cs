﻿namespace Models
{
    using System.Collections.Generic;

    public class Address
    {
        public long Id { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string Building { get; set; }
        public string Apartment { get; set; }
        public List<Person> Persons { get; set; }
        public AddressType AddressType { get; set; }
    }
}