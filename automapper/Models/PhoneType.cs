﻿namespace Models
{
    public enum PhoneType
    {
        Unknown,
        Home,
        Work,
        Mobile
    }
}