﻿namespace Models
{
    public class ViewModelUser
    {
        public string Id { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }
    }
}