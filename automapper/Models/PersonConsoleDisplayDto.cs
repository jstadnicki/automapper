﻿namespace Models
{
    using System.Collections.Generic;

    public class PersonConsoleDisplayDto
    {
        public string FirstName { get; set; }

        public IEnumerable<string> Countries { get; set; }

        public IEnumerable<string> Phone { get; set; }
    }
}