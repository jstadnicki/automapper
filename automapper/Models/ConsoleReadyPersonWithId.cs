﻿namespace Models
{
    public class ConsoleReadyPersonWithId : ConsoleReadyPerson
    {
        public long Id { get; set; }
    }
}