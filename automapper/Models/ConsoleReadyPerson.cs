﻿namespace Models
{
    public class ConsoleReadyPerson
    {
        public string Name { get; set; }

        public string Phone { get; set; }

        public string Country { get; set; }
    }
}