﻿namespace Models
{
    using System.Collections.Generic;

    public class Telephone
    {
        public long Id { get; set; }

        public string Number { get; set; }

        public PhoneType PhoneType { get; set; }

        public List<Person> Persons { get; set; }
    }
}