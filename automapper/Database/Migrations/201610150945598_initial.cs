namespace Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Country = c.String(),
                        City = c.String(),
                        Street = c.String(),
                        Building = c.String(),
                        Apartment = c.String(),
                        AddressType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.People",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Telephones",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Number = c.String(),
                        PhoneType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PersonAddresses",
                c => new
                    {
                        Person_Id = c.Long(nullable: false),
                        Address_Id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.Person_Id, t.Address_Id })
                .ForeignKey("dbo.People", t => t.Person_Id, cascadeDelete: true)
                .ForeignKey("dbo.Addresses", t => t.Address_Id, cascadeDelete: true)
                .Index(t => t.Person_Id)
                .Index(t => t.Address_Id);
            
            CreateTable(
                "dbo.TelephonePersons",
                c => new
                    {
                        Telephone_Id = c.Long(nullable: false),
                        Person_Id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.Telephone_Id, t.Person_Id })
                .ForeignKey("dbo.Telephones", t => t.Telephone_Id, cascadeDelete: true)
                .ForeignKey("dbo.People", t => t.Person_Id, cascadeDelete: true)
                .Index(t => t.Telephone_Id)
                .Index(t => t.Person_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TelephonePersons", "Person_Id", "dbo.People");
            DropForeignKey("dbo.TelephonePersons", "Telephone_Id", "dbo.Telephones");
            DropForeignKey("dbo.PersonAddresses", "Address_Id", "dbo.Addresses");
            DropForeignKey("dbo.PersonAddresses", "Person_Id", "dbo.People");
            DropIndex("dbo.TelephonePersons", new[] { "Person_Id" });
            DropIndex("dbo.TelephonePersons", new[] { "Telephone_Id" });
            DropIndex("dbo.PersonAddresses", new[] { "Address_Id" });
            DropIndex("dbo.PersonAddresses", new[] { "Person_Id" });
            DropTable("dbo.TelephonePersons");
            DropTable("dbo.PersonAddresses");
            DropTable("dbo.Telephones");
            DropTable("dbo.People");
            DropTable("dbo.Addresses");
        }
    }
}
