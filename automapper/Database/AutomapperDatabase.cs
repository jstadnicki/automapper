﻿namespace Database
{
    using System.Data.Entity;

    using Models;

    public class AutomapperDatabase : DbContext
    {
        public AutomapperDatabase()
            : base("automapper")
        {
            this.Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<Person> Persons { get; set; }

        public DbSet<Address> Addresses { get; set; }

        public DbSet<Telephone> Telephones { get; set; }
    }
}
