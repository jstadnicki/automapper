﻿namespace _001_NoAutomapper
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    using Database;

    public class Program
    {
        public static void Main(string[] args)
        {
            var db = new AutomapperDatabase();

            db.Database.Log = Console.WriteLine;

            var persons =
                db.Persons.Include(p => p.Addresses)
                    .Include(p => p.Telephones)
                    .Where(p => p.Addresses.Any())
                    .Where(p => p.Telephones.Any())
                    .Take(100)
                    .ToList();

            foreach (var person in persons)
            {
                Console.WriteLine(
                    $"Name:{person.FirstName.PadRight(20)}\t\t From:{person.Addresses.First().Country.PadRight(20)}\t\t Has phone: {person.Telephones.First().PhoneType.ToString().PadRight(20)}");
            }
        }
    }
}
