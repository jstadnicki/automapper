﻿namespace _004_ManualWithDto
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    using Database;

    using Models;

    class Program
    {
        static void Main(string[] args)
        {
            var db = new AutomapperDatabase();

            db.Database.Log = s => Console.WriteLine(s);

            var persons =
                db.Persons.Include(p => p.Addresses)
                    .Include(p => p.Telephones)
                    .Where(p => p.Addresses.Any())
                    .Where(p => p.Telephones.Any())
                    .Select(
                        p =>
                            new PersonConsoleDisplayDto
                            {
                                FirstName = p.FirstName,
                                Countries = p.Addresses.Select(a => a.Country),
                                Phone = p.Telephones.Select(t => t.PhoneType.ToString())
                            })
                    .Take(100)
                    .ToList()
                    .Select(
                        p =>
                            new ConsoleReadyPerson
                            {
                                Name = p.FirstName,
                                Phone = p.Phone.First().ToString(),
                                Country = p.Countries.First()
                            })
                            .ToList();

            foreach (var person in persons)
            {
                Console.WriteLine(
                    $"Name:{person.Name.PadRight(20)}\t\t From:{person.Country.PadRight(20)}\t\t Has phone: {person.Phone.PadRight(20)}");
            }
        }
    }
}
