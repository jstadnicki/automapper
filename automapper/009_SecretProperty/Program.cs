﻿namespace _009_SecretProperty
{
    using System;

    using AutoMapper;

    using Models;

    public class Program
    {
        public static void Main(string[] args)
        {
            Mapper.Initialize(c => c.CreateMap<DatabaseUser, ViewModelUser>().ReverseMap());
            #region fix
            // Mapper.Initialize(
            //    c => c.CreateMap<DatabaseUser, ViewModelUser>()
            //          .ForMember(d => d.Password, o => o.Ignore())
            //          .ReverseMap());
            #endregion
            var databaseUser = new DatabaseUser();

            databaseUser.Email = "jan@nowak.pl";
            databaseUser.Id = "50123";
            databaseUser.Password = "jak to plain tekstem";

            var viewModelUser = Mapper.Map<ViewModelUser>(databaseUser);
            var afterConversion = Mapper.Map<DatabaseUser>(viewModelUser);
            Console.WriteLine(afterConversion.Password);
        }
    }
}