﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _0090_AutomapperConvert
{
    using System.Data.Entity;

    using AutoMapper;

    using Database;

    using Models;

    class Program
    {
        static void Main(string[] args)
        {
            ConfigureAutomapper();

            var db = new AutomapperDatabase();

            db.Database.Log = s => Console.WriteLine(s);

            var persons =
                db.Persons.Include(p => p.Addresses)
                    .Include(p => p.Telephones)
                    .Where(p => p.Addresses.Any())
                    .Where(p => p.Telephones.Any())
                    .Take(100)
                    .ToList();

            var personsToDisplay = Mapper.Map<List<Person>, List<ConsoleReadyPerson>>(persons);

            foreach (var person in personsToDisplay)
            {
                Console.WriteLine(
                    $"Name:{person.Name.PadRight(30)}\t\t From:{person.Country.PadRight(20)}\t\t Has phone: {person.Phone.PadRight(20)}");
            }
        }

        private static void ConfigureAutomapper()
        {
            Mapper.Initialize(
                c =>
                    {
                        c.CreateMap<Person, ConsoleReadyPerson>().ConvertUsing<PersonToBeReadyForConsoleConverter>();
                    });
        }
    }

    internal class PersonToBeReadyForConsoleConverter : ITypeConverter<Person, ConsoleReadyPerson>
    {
        public ConsoleReadyPerson Convert(Person source, ConsoleReadyPerson destination, ResolutionContext context)
        {
            return new ConsoleReadyPerson
            {
                Country = source.Addresses.Select(s => s.Country).First(),
                Phone = source.Telephones.Select(p => p.PhoneType).First().ToString(),
                Name = source.FirstName
            };
        }
    }
}
