﻿namespace _003_ManualWithType
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    using Database;

    using Models;

    class Program
    {
        static void Main(string[] args)
        {
            var db = new AutomapperDatabase();

            db.Database.Log = s => Console.WriteLine(s);

            var persons =
                db.Persons
                .Include(p => p.Addresses)
                    .Include(p => p.Telephones)
                    .Where(p => p.Addresses.Any())
                    .Where(p => p.Telephones.Any())
                    .Select(p => new PersonConsoleDisplay
                    {
                        FirstName = p.FirstName,
                        Countries = p.Addresses.Select(a => a.Country),
                        Phone = p.Telephones.Select(t => t.PhoneType.ToString())
                    })
                    .Take(100)
                    .ToList();

            foreach (var person in persons)
            {
                Console.WriteLine(
                    $"Name:{person.FirstName.PadRight(20)}\t\t From:{person.Countries.First().PadRight(20)}\t\t Has phone: {person.Phone.First().PadRight(20)}");
            }
        }
    }
}
