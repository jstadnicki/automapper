﻿namespace Generator
{
    using System;
    using System.Collections.Generic;

    using Database;

    using Models;

    class Program
    {
        static void Main(string[] args)
        {
            var r = new Random(DateTime.Now.Millisecond);
            var db = new AutomapperDatabase();
            for (int i = 0; i < 10000; i++)
            {
                Console.WriteLine($"Creating person {i} of {10000}");
                var p = CreatePerson(r);
                db.Persons.Add(p);
                db.SaveChanges();
            }

            Console.WriteLine("saving to database");
            
        }

        private static Telephone CreateTelephone(Random random)
        {
            return new Telephone
            {
                Number = Faker.NumberFaker.Number(9).ToString(),
                PhoneType = (PhoneType)((random.Next() % 3) + 1),
                Persons = new List<Person>()
            };
        }

        private static Address CreateAddress(Random r)
        {
            return new Address
            {
                Country = Faker.LocationFaker.Country(),
                City = Faker.LocationFaker.City(),
                Street = Faker.LocationFaker.StreetName(),
                Building = Faker.LocationFaker.StreetNumber().ToString(),
                Apartment = Faker.LocationFaker.StreetNumber().ToString(),
                AddressType = (AddressType)((r.Next() % 3) + 1),
                Persons = new List<Person>()
            };
        }

        private static Person CreatePerson(Random r)
        {
            var person = new Person
            {
                Addresses = new List<Address>(),
                FirstName = Faker.NameFaker.FirstName(),
                LastName = Faker.NameFaker.LastName(),
                Telephones = new List<Telephone>()
            };
            for (int i = 0; i < r.Next() % 3; i++)
            {
                var a = CreateAddress(r);
                person.Addresses.Add(a);
            }

            for (int i = 0; i < r.Next() % 3; i++)
            {
                var t = CreateTelephone(r);
                person.Telephones.Add(t);
            }

            return person;
        }
    }
}
